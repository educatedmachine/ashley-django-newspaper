from django.db import models

# Create your models here.
class Article(models.Model):
    headline = models.CharField(max_length=255)
    publish_date = models.DateTimeField(auto_now_add=True)
    content = models.TextField()

    def __str__(self):
        return self.headline

class Background(models.Model):
    headline = models.CharField(max_length=255)
    content = models.TextField(blank=True, default="")
    order = models.IntegerField(default=0)
    article = models.ForeignKey(Article)

    class Meta:
        ordering = ["order",]

    def __str__(self):
        return self.headline

class Writer(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(default="writer@newspaper.com")
    bio = models.TextField(default="This writer is quite interesting but unspecific.")