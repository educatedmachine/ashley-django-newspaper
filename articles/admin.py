from django.contrib import admin

from .models import Article, Background, Writer


class BackgroundInline(admin.StackedInline):
    model = Background

class ArticleAdmin(admin.ModelAdmin):
    inlines = [BackgroundInline,]

# Register your models here.
admin.site.register(Article, ArticleAdmin)
admin.site.register(Background)
admin.site.register(Writer)