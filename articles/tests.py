from django.core.urlresolvers import reverse
from django.test import TestCase
from djangp.utils import timezone

# Create your tests here.
from .models import Article ,Background

class ArticleModelTests(TestCase):
    def test_article_creation(self):
        article = Article.objects.create(
            headline="The Day the Tests Stood Still",
            content="Testing has stopped."
        )
        now=timezone.now()
        self.assertLess(article.created_at, now)

class ArticleListViewTests(TestCase):
    def setUp(self):
        self.article = Article.objects.create(
            headline="From Tests with Love",
            content="Messages from tests."
        )
        self.article2 = Article.objects.create(
            headline="Day of the Tests",
            content="Today is testing day."
        )
        self.background = Background.objects.create(
            headline="Tests",
            content="What are tests?",
            article=self.article
        )
        #Generate multiple articles for "self.assertIn(self.article, resp.context['articles'])"
        for x in range(1, 3):
            Article.objects.create(
                writer=self.writer,
                headline='Article {}'.format(x),
                content='This article is numbered {}'.format(x),
                publish_date=datetime.datetime.today()
            )

    def test_article_list_view(self):
        resp = self.client.get(reverse('articles:list'))
        self.assertEqual(resp.status_code, 200)
        self.assertIn(self.article, resp.context['articles'])
        self.assertIn(self.article2, resp.context['articles'])
        self.assertTemplateUsed(resp, 'articles/articles_list.html')
        self.assertContains(resp, self.article.headline)

    def test_article_detail_view(self):
        resp = self.client.get(reverse('articles:detail', kwargs={'pk':self.article.pk}))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(self.course, resp.context['article'])

    def test_detail_template(self):
        '''Make sure the `articles/article_detail.html` template is used'''
        resp = self.client.get(reverse('articles:detail', kwargs={'pk':self.article.pk}))
        self.assertTemplateUsed(resp, 'articles/article_detail.html')
        
    def test_detail_template_writer(self):
        '''Make sure the article writer's name is in the rendered output'''
        resp = self.client.get(reverse('articles:detail', kwargs={'pk':self.article.pk}))
        self.assertContains(resp, self.article.writer.name)

    def test_background_detail_view(self):
        resp = self.client.get(reverse('articles:background', kwargs={
            'article_pk': self.article.pk,
            'background_pk': self.background.pk }))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(self.step, resp.context['background'])