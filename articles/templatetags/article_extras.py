from django import template

from articles.models import Article

register = template.Library()

@register.simple_tag
def latest_article():
    ''' Gets the most recent course that was added to the libreary. '''
    return Article.objects.latest('publish_date')

#register.simple_tag('latest_article')

@register.inclusion_tag('articles/article_nav.html')
def nav_articles_list():
    ''' Returns dictionary of articles to display as navigation pane '''
    articles = Article.objects.all()
    return {'articles': articles}

#register.inclusion_tag('articles/article_nav.html')(nav_articles_list)

@register.filter('time_estimate')
def time_estimate(word_count):
    ''' Estimates the number of minutes it will take to read a background article based on word count '''
    minutes = round(word_count/60)
    return minutes