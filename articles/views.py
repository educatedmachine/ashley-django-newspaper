from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render

from .models import Article, Background, Writer

# Create your views here.
def article_list(request):
    articles = Article.objects.all()
    return render(request, "articles/article_list.html", {"articles" : articles})

def article_detail(request, pk):
    article = get_object_or_404(Article, pk=pk)
    return render(request, "articles/article_detail.html", {"article" : article})

def background_detail(request, article_pk, background_pk):
    background = get_object_or_404(Background, article_id=article_pk, pk=background_pk)
    return render(request, "articles/background_detail.html", {"background" : background})

def writer_detail(request, pk):
    writer = Writer.objects.get(pk=pk)
    return render(request, "articles/writer_detail.html", {"writer" : writer})