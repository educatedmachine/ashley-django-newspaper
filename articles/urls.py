from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'(?P<article_pk>\d+)/(?P<background_pk>\d+)/$', views.background_detail, name="background"),
    url(r'(?P<pk>\d+)/$', views.article_detail, name="detail"),
    url(r'writer/(?P<pk>\d+)/$', views.writer_detail, name="writer"),
    url(r'^$', views.article_list, name="list"),
]